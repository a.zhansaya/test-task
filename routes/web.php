<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RegistrationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
   return redirect()->route('profiles.index');
});
Route::resource('profiles', ProfileController::class);

Route::controller(LoginController::class)->group(function () {
    Route::get('/login','form')->name('login');
    Route::post('/login','authenticate')->name('login.authenticate');
    Route::get('/logout', 'logout')->name('logout');
});

Route::controller(RegistrationController::class)->group(function() {
    Route::get('/registration', 'form')->name('registration.form');
    Route::post('/registration', 'register')->name('registration.register');
});
