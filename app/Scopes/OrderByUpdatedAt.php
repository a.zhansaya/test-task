<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderByUpdatedAt
 */
class OrderByUpdatedAt implements \Illuminate\Database\Eloquent\Scope
{
    /**
     * @param Builder $builder
     * @param Model $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->orderByDesc('updated_at');
    }
}
