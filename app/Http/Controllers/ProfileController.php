<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class ProfileController
 */
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'store', 'edit', 'update']);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $profiles = Profile::query()->paginate(10);

        return view('profiles.index', compact('profiles'));
    }

    /**
     * @param Request $request
     * @param Profile $profile
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Request $request, Profile $profile)
    {
        return view('profiles.show', compact('profile'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('profiles.create');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'fullname' => ['required'],
            'birthday' => ['required'],
            'age' => ['required'],
            'city' => ['required'],
            'education' => ['required'],
            'specialization' => ['required'],
            'skills' => ['required'],
        ]);

        $profile = Profile::create(
            array_merge($validated, ['user_id' => Auth::id()])
        );
        $request->session()->flash('result', 'Анкета успешно создана');

        return redirect()->route('profiles.show', ['profile' => $profile->id]);
    }

    /**
     * @param Request $request
     * @param Profile $profile
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Request $request, Profile $profile)
    {
        if (!$request->user() || $request->user()->cannot('update', $profile)) {
            abort(403);
        }

        return view('profiles.create', compact('profile'));
    }

    /**
     * @param Request $request
     * @param Profile $profile
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Profile $profile)
    {
        if (!$request->user() || $request->user()->cannot('update', $profile)) {
            abort(403);
        }

        $validated = $request->validate([
            'fullname' => ['required'],
            'birthday' => ['required'],
            'age' => ['required'],
            'city' => ['required'],
            'education' => ['required'],
            'specialization' => ['required'],
            'skills' => ['required'],
        ]);

        $profile->update($validated);
        $request->session()->flash('result', 'Анкета успешно обновлена');

        return redirect()->route('profiles.show', ['profile' => $profile->id]);
    }

    /**
     * @param Request $request
     * @param Profile $profile
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Profile $profile)
    {
        if (!$request->user() || $request->user()->cannot('delete', $profile)) {
            abort(403);
        }

        $profile->delete();
        $request->session()->flash('result', 'Анкета успешно удалена');

        return redirect()->route('profiles.index');
    }
}
