<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fullname' => $this->faker->name(),
            'birthday' => $this->faker->date(),
            'age' => $this->faker->numberBetween(18, 90),
            'city' => $this->faker->city(),
            'education' => $this->faker->words(5),
            'specialization' => $this->faker->realText(),
            'skills' => $this->faker->realText(),
        ];
    }
}
