@extends('layout.html')

@section('content')
    @include('layout.navbar')
    @php($profile = $profile ?? null)

    <h1 class="text-center">{{$profile ? 'Редактировать анкету' : 'Создать анкету'}}</h1>

    <form action="{{ $profile ? route('profiles.update', ['profile' => $profile->id]) : route('profiles.store') }}"
          method="post" class="my-5 mx-auto" style="max-width: 800px;">
        @if($profile)
            @method('PUT')
        @endif
        @csrf
        <div class="mb-3">
            <input type="text" id="fullname" name="fullname" class="form-control" required
                   value="{{ old('fullname') ?? $profile ? $profile->fullname : '' }}">
            <label for="fullname" class="small fw-lighter ms-2">ФИО</label>
        </div>
        <div class="mb-3">
            <input type="text" id="birthday" name="birthday" class="form-control" required
                   onfocus="(this.type='date')" onblur="(this.type='text')"
                   value="{{ old('birthday') ?? $profile ? $profile->birthday : '' }}">
            <label for="birthday" class="small fw-lighter ms-2">Дата рождения</label>
        </div>
        <div class="mb-3">
            <input type="number" id="age" name="age" class="form-control" required
                   value="{{ old('age') ?? $profile ? $profile->age : '' }}">
            <label for="age" class="small fw-lighter ms-2">Возраст</label>
        </div>
        <div class="mb-3">
            <input type="text" id="city" name="city" class="form-control" required
                   value="{{ old('city') ?? $profile ? $profile->city : '' }}">
            <label for="city" class="small fw-lighter ms-2">Город</label>
        </div>
        <div class="mb-3">
            <textarea name="education" id="education" cols="30" rows="5" class="form-control" required
            >{{ old('education') ?? $profile ? $profile->education : '' }}</textarea>
            <label for="education" class="small fw-lighter ms-2">Образование</label>
        </div>
        <div class="mb-3">
            <input type="text" id="specialization" name="specialization" class="form-control" required
                   value="{{ old('specialization') ?? $profile ? $profile->specialization : '' }}">
            <label for="specialization" class="small fw-lighter ms-2">Специализация</label>
        </div>
        <div class="mb-3">
            <textarea name="skills" id="skills" cols="30" rows="5" class="form-control" required
            >{{ old('skills') ?? $profile ? $profile->skills : '' }}</textarea>
            <label for="skills" class="small fw-lighter ms-2">Профессиональные навыки</label>
        </div>
        <div class="d-grid">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
