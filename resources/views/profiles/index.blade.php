@extends('layout.html')

@section('content')
    @include('layout.navbar')
    @if(session('result'))
        <div class="alert alert-success" role="alert">{{ session('result') }}</div>
    @endif
    <h3 class="text-center mb-4">Все анкеты</h3>
    @foreach($profiles as $profile)
        <a href="{{ route('profiles.show', ['profile' => $profile->id]) }}" class="card shadow-sm mb-4"
           style="text-decoration: unset; color: unset">
            <div class="card-header">{{ $profile->specialization }}</div>
            <div class="card-body">
                <h5 class="card-title">{{ $profile->fullname }}</h5>
                <p>{{ $profile->education }}</p>
                <p class="small text-end m-0">{{ $profile->updated_at->format('Y-m-d') }}</p>
            </div>
        </a>
    @endforeach
    {{ $profiles->links() }}
@endsection
