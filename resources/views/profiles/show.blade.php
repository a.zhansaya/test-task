@extends('layout.html')

@section('content')
    @include('layout.navbar')
    @if(session('result'))
        <div class="alert alert-success" role="alert">{{ session('result') }}</div>
    @endif
    <div class="p-3">
        <h1 class="display-5 mb-4 ps-4">{{ $profile->fullname }}</h1>
        <div>
            <p class="fw-bolder">Специализация</p>
            <p class="fw-light">{{ $profile->specialization }}</p>
        </div>
        <div>
            <p class="fw-bolder">Навыки</p>
            <p class="fw-light">{{ $profile->skills }}</p>
        </div>
        <div>
            <p class="fw-bolder">Образование</p>
            <p class="fw-light">{{ $profile->education }}</p>
        </div>
        <div>
            <p class="fw-bolder">Город</p>
            <p class="fw-light">{{ $profile->city }}</p>
        </div>
        <div>
            <p class="fw-bolder">Возраст</p>
            <p class="fw-light">{{ $profile->age }}</p>
        </div>
        <div>
            <p class="fw-bolder">Дата рождения</p>
            <p class="fw-light">{{ $profile->birthday }}</p>
        </div>
    </div>
    @if(\Illuminate\Support\Facades\Auth::id() == $profile->user_id)
        <a href="{{ route('profiles.edit', ['profile' => $profile->id]) }}" class="me-4">Редактировать</a>
        <form action="{{ route('profiles.destroy', ['profile' => $profile->id]) }}" method="post" class="d-inline">
            @csrf
            @method('delete')
            <button class="btn btn-link">Удалить</button>
        </form>
    @endif
@endsection
