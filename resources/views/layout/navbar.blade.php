<nav class="navbar navbar-expand-lg justify-content-between mb-4">
    <a href="{{ route('profiles.index') }}" class="navbar-brand">Главная</a>
    <div class="navbar-nav">
        @if(\Illuminate\Support\Facades\Auth::check())
            @if(\Illuminate\Support\Facades\Auth::user()->profile)
                <a href="{{ route('profiles.show', ['profile' => \Illuminate\Support\Facades\Auth::user()->profile->id]) }}" class="nav-link">Моя анкета</a>
            @else
                <a href="{{ route('profiles.create') }}" class="nav-link">Создать анкету</a>
            @endif
            <a href="{{ route('logout') }}" class="nav-link">Выйти</a>
        @else
            <a href="{{ route('login') }}" class="nav-link">Войти</a>
        @endif
    </div>
</nav>
