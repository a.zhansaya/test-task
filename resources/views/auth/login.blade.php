@extends('layout.html')

@section('content')
    @include('layout.navbar')

    <div>
        @if(session('result'))
            <div class="alert alert-success" role="alert">{{ session('result') }}</div>
        @endif
        <h1 class="text-center text-secondary" style="margin-top: 20vh">Логин</h1>

        <form action="{{ route('login.authenticate') }}" method="post" class="mx-auto" style="max-width: 600px;">
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
            @endif
            @csrf
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" id="email" name="email" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Пароль</label>
                <input type="password" id="password" name="password" class="form-control" required>
            </div>
            <div class="d-grid mb-3">
                <button type="submit" class="btn btn-primary">Войти</button>
            </div>
            <p class="text-center">Нет аккаунта? <a href="{{ route('registration.form') }}">Зарегистрируйтесь</a></p>
        </form>
    </div>
@endsection
