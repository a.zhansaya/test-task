@extends('layout.html')

@section('content')
    @include('layout.navbar')

    <div>
        <h1 class="text-center text-secondary" style="margin-top: 10vh">Регистрация</h1>

        <form action="{{ route('registration.register') }}" method="post" class="mx-auto" style="max-width: 600px;">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Имя</label>
                <input type="text" id="name" name="name" class="form-control" required value="{{ old('name') }}">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" id="email" name="email" class="form-control" required value="{{ old('email') }}">
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Пароль</label>
                <input type="password" id="password" name="password" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="password_confirmation" class="form-label">Подтверждение пароля</label>
                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control"
                       required>
            </div>
            <div class="d-grid mb-3">
                <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
            </div>
            <p class="text-center">Уже зарегистрированы? <a href="{{ route('login') }}">Войдите</a></p>
        </form>
    </div>
@endsection
